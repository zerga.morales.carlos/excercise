package com.technical.configuration.validations;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = RangeDateValidator.class)
@Documented
public @interface RangeDateValidation {

    String message() default "Your end range should be higher than your start range";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String sourceFieldName();
    String destinyFieldName();
}
