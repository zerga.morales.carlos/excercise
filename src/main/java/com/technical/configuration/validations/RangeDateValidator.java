package com.technical.configuration.validations;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;

public class RangeDateValidator implements ConstraintValidator<RangeDateValidation, Object> {

    private String sourceFieldName;
    private String destinyFieldName;

    @Override
    public void initialize(RangeDateValidation annotation) {
        sourceFieldName = annotation.sourceFieldName();
        destinyFieldName = annotation.destinyFieldName();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext ctx) {

        if (value == null) {
            return true;
        }

        try {
            String sourceValue = BeanUtils.getProperty(value, sourceFieldName);
            String destinyValue = BeanUtils.getProperty(value, destinyFieldName);


            if (sourceValue != null && destinyValue != null && Period.between(LocalDate.parse(sourceValue), LocalDate.parse(destinyValue)).getDays() <= 0) {
                return false;
            }

        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }

        return true;
    }
}