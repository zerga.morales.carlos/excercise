package com.technical.configuration.web;

import com.technical.exercise.business.exception.BusinessException;
import com.technical.exercise.business.exception.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice
public class WebErrorRestHandler {

    @ExceptionHandler
    public ResponseEntity<ExceptionResponse> handleFormError(MethodArgumentNotValidException methodArgumentNotValidException) {
        List<ValidationResponse> errors = new ArrayList<>();
        for (FieldError error : methodArgumentNotValidException.getBindingResult().getFieldErrors()) {
            errors.add(ValidationResponse.builder()
                    .field(error.getField())
                    .message(error.getDefaultMessage())
                    .build());
        }
        for (ObjectError error : methodArgumentNotValidException.getBindingResult().getGlobalErrors()) {
            errors.add(ValidationResponse.builder()
                    .field(error.getObjectName())
                    .message(error.getDefaultMessage())
                    .build());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(ExceptionResponse.builder()
                        .code("GEN_01")
                        .message("Your request has some validation errors")
                        .validations(errors)
                        .build());
    }

    @ExceptionHandler
    public ResponseEntity<ExceptionResponse> handleBusinessException(
            BusinessException businessException) {
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY)
                .body(ExceptionResponse.builder()
                        .code(businessException.getCode())
                        .message(businessException.getMessage())
                        .build());
    }

    @ExceptionHandler
    public ResponseEntity<ExceptionResponse> handleEntityNotFoundException(
            EntityNotFoundException entityNotFoundException) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(ExceptionResponse.builder()
                        .code("ENT_NOT_FOUND")
                        .message(String.format("Entity with name '%s' and id '%s' is not found", entityNotFoundException.getClazz().getSimpleName(), entityNotFoundException.getId()))
                        .build());
    }
}
