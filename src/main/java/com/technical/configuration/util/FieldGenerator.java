package com.technical.configuration.util;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;

@Component
public class FieldGenerator {

    public LocalDateTime now() {
        return LocalDateTime.now(ZoneId.of("Europe/Paris"));
    }
}
