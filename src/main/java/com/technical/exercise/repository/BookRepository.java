package com.technical.exercise.repository;

import com.technical.exercise.business.domain.BookStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Repository
public interface BookRepository extends JpaRepository<BookEntity, UUID>,
        JpaSpecificationExecutor<BookEntity> {

    PropertyBlockEntity findFirstByPropertyIdAndIdAndEnabledTrue(UUID propertyId, UUID id);

    @Query("SELECT bp from book bp where bp.propertyId = :propertyId and ( bp.startDate between :startDate and :endDate or :startDate <= bp.endDate) and bp.enabled and bp.status = :status ")
    List<BookEntity> existsAnyBook(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate, @Param("propertyId") UUID propertyId, @Param("status") BookStatus status);

    BookEntity findFirstByIdAndEnabledTrue(UUID bookId);
}
