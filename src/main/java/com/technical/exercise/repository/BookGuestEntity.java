package com.technical.exercise.repository;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BookGuestEntity {
    private String firstName;

    private String lastName;

    private String email;

    private String phoneNumber;

    private String comment;
}
