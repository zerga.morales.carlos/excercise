package com.technical.exercise.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Repository
public interface PropertyBlockRepository extends JpaRepository<PropertyBlockEntity, UUID>,
        JpaSpecificationExecutor<PropertyBlockEntity> {

    @Query("SELECT bp from property_block bp where bp.propertyId = :propertyId and ( bp.startDate between :startDate and :endDate or :startDate <= bp.endDate) and bp.enabled ")
    List<PropertyBlockEntity> existsAnyBlock(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate, @Param("propertyId") UUID propertyId);

    PropertyBlockEntity findFirstByPropertyIdAndIdAndEnabledTrue(UUID propertyId, UUID id);
}
