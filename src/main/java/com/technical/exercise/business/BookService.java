package com.technical.exercise.business;


import com.technical.configuration.util.FieldGenerator;
import com.technical.exercise.business.domain.Book;
import com.technical.exercise.business.domain.BookGuest;
import com.technical.exercise.business.domain.BookStatus;
import com.technical.exercise.business.exception.BookAlreadyCancelledException;
import com.technical.exercise.business.exception.BusinessException;
import com.technical.exercise.business.exception.EntityNotFoundException;
import com.technical.exercise.business.exception.OnlyAllowedToReviveCancelledBookException;
import com.technical.exercise.business.exception.PropertyAlreadyBookedException;
import com.technical.exercise.repository.BookEntity;
import com.technical.exercise.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BookService {

    private final BookRepository bookRepository;

    private final PropertyMapper propertyMapper;

    private final FieldGenerator fieldGenerator;

    public Book createBook(Book book) throws BusinessException {

        if (!this.bookRepository.existsAnyBook(book.getStartDate(), book.getEndDate(), book.getPropertyId(), BookStatus.PENDING).isEmpty()) {
            throw new PropertyAlreadyBookedException(book.getPropertyId(), book.getStartDate(), book.getEndDate());
        }

        BookEntity bookEntity = this.propertyMapper.convertDomainToDatabase(book);
        bookEntity.setCreatedAt(this.fieldGenerator.now());
        bookEntity.setEnabled(true);
        bookEntity.setStatus(BookStatus.PENDING);

        return this.propertyMapper.convertDatabaseToDomain(
                this.bookRepository.saveAndFlush(bookEntity)
        );
    }

    public Book getBook(UUID bookId) throws EntityNotFoundException {

        BookEntity persistedBook = this.bookRepository.findFirstByIdAndEnabledTrue(bookId);

        if (persistedBook == null) {
            throw new EntityNotFoundException(BookEntity.class, bookId);
        }
        return this.propertyMapper.convertDatabaseToDomain(persistedBook);
    }

    public Book updateBookDates(UUID bookId, LocalDate startDate, LocalDate endDate) throws EntityNotFoundException, BusinessException {

        BookEntity persistedBook = this.bookRepository.findFirstByIdAndEnabledTrue(bookId);

        if (persistedBook == null) {
            throw new EntityNotFoundException(BookEntity.class, bookId);
        }

        if (persistedBook.getStatus().equals(BookStatus.CANCELLED)) {
            throw new BookAlreadyCancelledException(bookId);
        }

        List<BookEntity> allBooks = this.bookRepository.existsAnyBook(startDate, endDate, persistedBook.getPropertyId(), BookStatus.PENDING);
        if (allBooks.size() >= 2) {
            throw new PropertyAlreadyBookedException(persistedBook.getPropertyId(), startDate, endDate);
        } else if (!allBooks.isEmpty() && !allBooks.get(0).getId().equals(bookId)) {
            throw new PropertyAlreadyBookedException(persistedBook.getPropertyId(), startDate, endDate);
        }

        persistedBook.setStartDate(startDate);
        persistedBook.setEndDate(endDate);
        return this.propertyMapper.convertDatabaseToDomain(
                this.bookRepository.saveAndFlush(persistedBook)
        );
    }

    public Book updateBookGuest(UUID bookId, BookGuest bookGuest) throws EntityNotFoundException, BusinessException {
        BookEntity persistedBook = this.bookRepository.findFirstByIdAndEnabledTrue(bookId);

        if (persistedBook == null) {
            throw new EntityNotFoundException(BookEntity.class, bookId);
        }

        if (persistedBook.getStatus().equals(BookStatus.CANCELLED)) {
            throw new BookAlreadyCancelledException(bookId);
        }

        persistedBook.setGuest(
                this.propertyMapper.copy(
                        this.propertyMapper.convertDomainToDatabase(bookGuest),
                        persistedBook.getGuest()
                )
        );

        return this.propertyMapper.convertDatabaseToDomain(
                this.bookRepository.saveAndFlush(persistedBook)
        );
    }

    public void deleteBook(UUID bookId) throws EntityNotFoundException {
        BookEntity persistedBook = this.bookRepository.findFirstByIdAndEnabledTrue(bookId);

        if (persistedBook == null) {
            throw new EntityNotFoundException(BookEntity.class, bookId);
        }
        persistedBook.setEnabled(false);
        this.bookRepository.saveAndFlush(persistedBook);
    }

    public Book cancelBook(UUID bookId) throws EntityNotFoundException, BookAlreadyCancelledException {

        BookEntity persistedBook = this.bookRepository.findFirstByIdAndEnabledTrue(bookId);

        if (persistedBook == null) {
            throw new EntityNotFoundException(BookEntity.class, bookId);
        }

        if (persistedBook.getStatus().equals(BookStatus.CANCELLED)) {
            throw new BookAlreadyCancelledException(bookId);
        }

        persistedBook.setStatus(BookStatus.CANCELLED);

        return this.propertyMapper.convertDatabaseToDomain(
                this.bookRepository.saveAndFlush(persistedBook)
        );
    }

    public Book getCancelledBook(UUID bookId) throws EntityNotFoundException, BusinessException {
        BookEntity persistedBook = this.bookRepository.findFirstByIdAndEnabledTrue(bookId);

        if (persistedBook == null) {
            throw new EntityNotFoundException(BookEntity.class, bookId);
        }
        if (persistedBook.getStatus().equals(BookStatus.PENDING)) {
            throw new OnlyAllowedToReviveCancelledBookException(bookId);
        }

        return this.propertyMapper.convertDatabaseToDomain(persistedBook);
    }


    public void validateNoneBooks(UUID propertyId, LocalDate startDate, LocalDate endDate) throws BusinessException {

        if (!this.bookRepository.existsAnyBook(startDate, endDate, propertyId, BookStatus.PENDING).isEmpty()) {
            throw new PropertyAlreadyBookedException(propertyId, startDate, endDate);
        }
    }
}
