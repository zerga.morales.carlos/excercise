package com.technical.exercise.business.exception;

import java.time.LocalDate;
import java.util.UUID;

public class PropertyAlreadyBookedException extends BusinessException {

    public PropertyAlreadyBookedException(UUID propertyId, LocalDate startDate, LocalDate endDate) {
        super("BOO_01", String.format("Property with id '%s' is already booked within these days '%s' and '%s'", propertyId, startDate, endDate));
    }
}
