package com.technical.exercise.business.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

@AllArgsConstructor
@Getter
public class EntityNotFoundException extends Exception {


    private Class<?> clazz;

    private UUID id;
}
