package com.technical.exercise.business.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class BusinessException extends Exception {

    private String code;

    private String message;
}
