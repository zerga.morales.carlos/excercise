package com.technical.exercise.business.exception;

import java.time.LocalDate;
import java.util.UUID;

public class PropertyAlreadyBlockedException extends BusinessException {

    public PropertyAlreadyBlockedException(UUID propertyId, LocalDate startDate, LocalDate endDate) {
        super("BLK_01", String.format("Property with id '%s' is already locked within these days '%s' and '%s'", propertyId, startDate, endDate));
    }
}
