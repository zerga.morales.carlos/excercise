package com.technical.exercise.business.exception;

import java.util.UUID;

public class BookAlreadyCancelledException extends BusinessException {

    public BookAlreadyCancelledException(UUID bookId) {
        super("BLK_01", String.format("Book with id '%s' is already canceled and no change can be made", bookId));
    }
}
