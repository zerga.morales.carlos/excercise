package com.technical.exercise.business.exception;

import java.util.UUID;

public class OnlyAllowedToReviveCancelledBookException extends BusinessException{
    public OnlyAllowedToReviveCancelledBookException(UUID bookId) {
        super("BOK_01", String.format("Book with id '%s' is on pending and can't be revived", bookId));
    }
}
