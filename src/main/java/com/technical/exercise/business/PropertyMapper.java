package com.technical.exercise.business;

import com.technical.exercise.business.domain.Book;
import com.technical.exercise.business.domain.BookGuest;
import com.technical.exercise.business.domain.Property;
import com.technical.exercise.business.domain.PropertyBlock;
import com.technical.exercise.controller.request.CreateBookRequest;
import com.technical.exercise.controller.request.CreatePropertyRequest;
import com.technical.exercise.controller.request.UpdateBookDatesRequest;
import com.technical.exercise.controller.request.UpdateBookGuestRequest;
import com.technical.exercise.repository.BookEntity;
import com.technical.exercise.repository.BookGuestEntity;
import com.technical.exercise.repository.PropertyBlockEntity;
import com.technical.exercise.repository.PropertyEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface PropertyMapper {
    Property convertRequestToDomain(CreatePropertyRequest createPropertyRequest);

    PropertyEntity convertDomainToDatabase(Property property);

    Property convertDatabaseToDomain(PropertyEntity propertyEntity);

    PropertyBlock convertDatabaseToDomain(PropertyBlockEntity propertyBlockEntity);

    Book mapRequestToDomain(CreateBookRequest createBookRequest);

    BookEntity convertDomainToDatabase(Book book);

    Book convertDatabaseToDomain(BookEntity bookEntity);

    Book convertRequestToDomain(UpdateBookDatesRequest updateBookRequest);

    BookGuest mapRequestToDomain(UpdateBookGuestRequest updateBookDatesRequest);

    BookGuestEntity copy(BookGuestEntity bookGuest, @MappingTarget BookGuestEntity bookGuestEntity);

    BookGuestEntity convertDomainToDatabase(BookGuest bookGuest);
}
