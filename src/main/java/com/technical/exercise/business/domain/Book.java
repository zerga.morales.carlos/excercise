package com.technical.exercise.business.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Book {

    private UUID id;

    private UUID propertyId;

    private BookStatus status;

    private LocalDate startDate;

    private LocalDate endDate;

    private BookGuest guest;
}
