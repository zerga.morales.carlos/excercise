package com.technical.exercise.business.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.UUID;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PropertyBlock {

    private UUID id;

    private UUID propertyId;

    private LocalDate startDate;

    private LocalDate endDate;
}
