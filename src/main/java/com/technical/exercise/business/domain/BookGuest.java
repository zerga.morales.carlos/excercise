package com.technical.exercise.business.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BookGuest {

    private String firstName;

    private String lastName;

    private String email;

    private String phoneNumber;

    private String comment;
}
