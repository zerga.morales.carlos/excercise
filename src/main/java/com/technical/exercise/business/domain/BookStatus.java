package com.technical.exercise.business.domain;

public enum BookStatus {

    PENDING,
    CANCELLED;
}
