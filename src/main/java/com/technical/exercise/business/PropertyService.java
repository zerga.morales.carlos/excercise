package com.technical.exercise.business;

import com.technical.configuration.util.FieldGenerator;
import com.technical.exercise.business.domain.Property;
import com.technical.exercise.business.domain.PropertyBlock;
import com.technical.exercise.business.exception.BusinessException;
import com.technical.exercise.business.exception.EntityNotFoundException;
import com.technical.exercise.business.exception.PropertyAlreadyBlockedException;
import com.technical.exercise.repository.PropertyBlockEntity;
import com.technical.exercise.repository.PropertyBlockRepository;
import com.technical.exercise.repository.PropertyEntity;
import com.technical.exercise.repository.PropertyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PropertyService {

    private final PropertyRepository propertyRepository;

    private final PropertyBlockRepository propertyBlockRepository;

    private final PropertyMapper propertyMapper;

    private final FieldGenerator fieldGenerator;

    public Property createProperty(Property property) {

        PropertyEntity propertyEntity = this.propertyMapper.convertDomainToDatabase(property);
        propertyEntity.setCreatedAt(this.fieldGenerator.now());

        return this.propertyMapper.convertDatabaseToDomain(
                this.propertyRepository.saveAndFlush(propertyEntity)
        );
    }

    public PropertyBlock createBlock(UUID propertyId, LocalDate startDate, LocalDate endDate) throws BusinessException, EntityNotFoundException {

        if (!this.propertyRepository.existsById(propertyId)) {
            throw new EntityNotFoundException(PropertyEntity.class, propertyId);
        }

        if (!this.propertyBlockRepository.existsAnyBlock(startDate, endDate, propertyId).isEmpty()) {
            throw new PropertyAlreadyBlockedException(propertyId, startDate, endDate);
        }
        PropertyBlockEntity propertyEntityBlock = new PropertyBlockEntity();
        propertyEntityBlock.setEnabled(true);
        propertyEntityBlock.setCreatedAt(this.fieldGenerator.now());
        propertyEntityBlock.setStartDate(startDate);
        propertyEntityBlock.setPropertyId(propertyId);
        propertyEntityBlock.setEndDate(endDate);
        return this.propertyMapper.convertDatabaseToDomain(
                this.propertyBlockRepository.saveAndFlush(propertyEntityBlock)
        );
    }

    public PropertyBlock updateBlock(UUID propertyId, UUID blockId, LocalDate startDate, LocalDate endDate) throws BusinessException, EntityNotFoundException {

        if (!this.propertyRepository.existsById(propertyId)) {
            throw new EntityNotFoundException(PropertyEntity.class, propertyId);
        }

        PropertyBlockEntity persistedBlock = this.propertyBlockRepository.findFirstByPropertyIdAndIdAndEnabledTrue(propertyId, blockId);

        if (persistedBlock == null) {
            throw new EntityNotFoundException(PropertyBlockEntity.class, blockId);
        }

        List<PropertyBlockEntity> allBlocks = this.propertyBlockRepository.existsAnyBlock(startDate, endDate, propertyId);
        if (allBlocks.size() >= 2) {
            throw new PropertyAlreadyBlockedException(propertyId, startDate, endDate);
        } else if (!allBlocks.isEmpty() && !allBlocks.get(0).getId().equals(blockId)) {
            throw new PropertyAlreadyBlockedException(propertyId, startDate, endDate);
        }


        persistedBlock.setStartDate(startDate);
        persistedBlock.setEndDate(endDate);
        return this.propertyMapper.convertDatabaseToDomain(
                this.propertyBlockRepository.saveAndFlush(persistedBlock)
        );
    }

    public void deleteBlock(UUID propertyId, UUID blockId) throws EntityNotFoundException {

        if (!this.propertyRepository.existsById(propertyId)) {
            throw new EntityNotFoundException(PropertyEntity.class, propertyId);
        }

        PropertyBlockEntity persistedBlock = this.propertyBlockRepository.findFirstByPropertyIdAndIdAndEnabledTrue(propertyId, blockId);

        if (persistedBlock == null) {
            throw new EntityNotFoundException(PropertyBlockEntity.class, blockId);
        }
        persistedBlock.setEnabled(false);
        this.propertyBlockRepository.saveAndFlush(persistedBlock);
    }

    public void validateNoneBlocks(UUID propertyId, LocalDate startDate, LocalDate endDate) throws PropertyAlreadyBlockedException, EntityNotFoundException {

        if (!this.propertyRepository.existsById(propertyId)) {
            throw new EntityNotFoundException(PropertyEntity.class, propertyId);
        }

        if (!this.propertyBlockRepository.existsAnyBlock(startDate, endDate, propertyId).isEmpty()) {
            throw new PropertyAlreadyBlockedException(propertyId, startDate, endDate);
        }
    }
}
