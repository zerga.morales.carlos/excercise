package com.technical.exercise.controller.request;


import com.technical.configuration.validations.RangeDateValidation;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@RangeDateValidation(
        sourceFieldName = "startDate",
        destinyFieldName = "endDate")
public class UpdateBookDatesRequest {

    @NotNull
    private LocalDate startDate;

    @NotNull
    private LocalDate endDate;
}
