package com.technical.exercise.controller;

import com.technical.exercise.business.BookService;
import com.technical.exercise.business.PropertyMapper;
import com.technical.exercise.business.PropertyService;
import com.technical.exercise.business.domain.Property;
import com.technical.exercise.business.domain.PropertyBlock;
import com.technical.exercise.business.exception.BusinessException;
import com.technical.exercise.business.exception.EntityNotFoundException;
import com.technical.exercise.controller.request.CreateBlockRequest;
import com.technical.exercise.controller.request.CreatePropertyRequest;
import com.technical.exercise.controller.request.UpdateBlockRequest;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/property")
@RequiredArgsConstructor
public class PropertyController {

    private final PropertyService propertyService;

    private final BookService bookService;

    private final PropertyMapper propertyMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Property createProperty(@Valid @RequestBody CreatePropertyRequest createPropertyRequest) {
        return this.propertyService.createProperty(
                this.propertyMapper.convertRequestToDomain(createPropertyRequest)
        );
    }

    @PostMapping("/{propertyId}/block")
    @ResponseStatus(HttpStatus.CREATED)
    public PropertyBlock createBlock(@PathVariable("propertyId") UUID propertyId,
                                     @Valid @RequestBody CreateBlockRequest createBlockRequest) throws BusinessException, EntityNotFoundException {

        this.bookService.validateNoneBooks(propertyId, createBlockRequest.getStartDate(), createBlockRequest.getEndDate());

        return this.propertyService.createBlock(propertyId,
                createBlockRequest.getStartDate(), createBlockRequest.getEndDate()
        );
    }

    @PutMapping("/{propertyId}/block/{blockId}")
    @ResponseStatus(HttpStatus.OK)
    public PropertyBlock updateBlock(@PathVariable("propertyId") UUID propertyId,
                                     @PathVariable("blockId") UUID blockId,
                                     @Valid @RequestBody UpdateBlockRequest createBlockRequest) throws BusinessException, EntityNotFoundException {

        this.bookService.validateNoneBooks(propertyId, createBlockRequest.getStartDate(), createBlockRequest.getEndDate());

        return this.propertyService.updateBlock(propertyId, blockId,
                createBlockRequest.getStartDate(), createBlockRequest.getEndDate()
        );
    }

    @DeleteMapping("/{propertyId}/block/{blockId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteBlock(@PathVariable("propertyId") UUID propertyId,
                            @PathVariable("blockId") UUID blockId) throws EntityNotFoundException {
        this.propertyService.deleteBlock(propertyId, blockId);
    }
}
