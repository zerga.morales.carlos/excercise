package com.technical.exercise.controller;

import com.technical.exercise.business.BookService;
import com.technical.exercise.business.PropertyMapper;
import com.technical.exercise.business.PropertyService;
import com.technical.exercise.business.domain.Book;
import com.technical.exercise.business.exception.BusinessException;
import com.technical.exercise.business.exception.EntityNotFoundException;
import com.technical.exercise.controller.request.CreateBookRequest;
import com.technical.exercise.controller.request.UpdateBookDatesRequest;
import com.technical.exercise.controller.request.UpdateBookGuestRequest;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/book")
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    private final PropertyService propertyService;

    private final PropertyMapper propertyMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Book createBook(@Valid @RequestBody CreateBookRequest createBookRequest) throws BusinessException, EntityNotFoundException {

        this.propertyService.validateNoneBlocks(createBookRequest.getPropertyId(), createBookRequest.getStartDate(), createBookRequest.getEndDate());

        return this.bookService.createBook(
                this.propertyMapper.mapRequestToDomain(createBookRequest)
        );
    }

    @PutMapping("/{bookId}/dates")
    @ResponseStatus(HttpStatus.CREATED)
    public Book updateBookDates(@PathVariable("bookId") UUID bookId,
                                @RequestBody UpdateBookDatesRequest updateBookDatesRequest) throws BusinessException, EntityNotFoundException {

        Book book = this.bookService.getBook(bookId);

        this.propertyService.validateNoneBlocks(book.getPropertyId(), updateBookDatesRequest.getStartDate(), updateBookDatesRequest.getEndDate());

        return this.bookService.updateBookDates(bookId,
                updateBookDatesRequest.getStartDate(), updateBookDatesRequest.getEndDate()
        );
    }

    @PutMapping("/{bookId}/guest")
    @ResponseStatus(HttpStatus.CREATED)
    public Book updateBookGuest(@PathVariable("bookId") UUID bookId,
                                @Valid @RequestBody UpdateBookGuestRequest updateBookDatesRequest) throws EntityNotFoundException, BusinessException {

        return this.bookService.updateBookGuest(bookId,
                this.propertyMapper.mapRequestToDomain(updateBookDatesRequest)
        );
    }

    @GetMapping("/{bookId}")
    @ResponseStatus(HttpStatus.OK)
    public Book getBook(@PathVariable("bookId") UUID bookId) throws EntityNotFoundException {

        return this.bookService.getBook(bookId);
    }

    @DeleteMapping("/{bookId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteBook(@PathVariable("bookId") UUID bookId) throws EntityNotFoundException {

        this.bookService.deleteBook(bookId);
    }

    @PostMapping("/{bookId}/cancel")
    @ResponseStatus(HttpStatus.OK)
    public Book createBook(@PathVariable("bookId") UUID bookId) throws BusinessException, EntityNotFoundException {

        return this.bookService.cancelBook(
                bookId
        );
    }

    @PostMapping("/{bookId}/revive")
    @ResponseStatus(HttpStatus.OK)
    public Book reviveBook(@PathVariable("bookId") UUID bookId) throws BusinessException, EntityNotFoundException {

        Book book = this.bookService.getCancelledBook(bookId);

        this.propertyService.validateNoneBlocks(book.getPropertyId(), book.getStartDate(), book.getEndDate());

        return this.bookService.createBook(
                book
        );
    }


}
