# Book Api

We have major field validations and all requirements are meet, also I provided a small example of swagger documentation located on http://localhost:8080/swagger-ui/index.html#/

Additionally, I added some documentation for the api


## Property Api Documentation

1.- __Create Property__

In order to create all booking ecosystem we should be able to create at least a property with only name

#### Request
```
curl --location 'localhost:8080/property' \
--header 'Content-Type: application/json' \
--data '{
"name":"Propiedad Peruana"
}'
```
#### Response
```
{
    "id": "0250e564-965d-485c-8e39-8a00f810c84c",
    "name": "Propiedad Peruana",
    "blocks": null
}
```

2- __Create a block in Property__

In order to a block into a property we have three main validation, block date doesn't exist, property exists and booking in that date doesn't exist

#### Request
```
curl --location 'localhost:8080/property/0250e564-965d-485c-8e39-8a00f810c84c/block' \
--header 'Content-Type: application/json' \
--data '{
    "startDate":"2023-12-14",
    "endDate":"2023-12-13"
}'
```
#### Response
```
{
    "id": "ded485f8-630a-4dc8-a4ff-afdffadb2472",
    "propertyId": "d5e1af33-cfbd-4e89-97cd-591c96eb2a60",
    "startDate": "2023-12-19",
    "endDate": "2023-12-20"
}
``````

3- __Update a block in Property__

In order to update a block from a property we have three main validation, block date doesn't exist, property exists and booking in that date doesn't exist considering old persisted block

#### Request
```
curl --location --request PUT 'localhost:8080/property/a257d3cf-7051-46f0-a66a-4d82ce7098f7/block/c514e0c5-6676-4e33-a8d2-c92007d67812' \
--header 'Content-Type: application/json' \
--data '{
    "startDate":"2023-12-14",
    "endDate":"2023-12-16"
}''
```
#### Response
```
{
    "id": "ded485f8-630a-4dc8-a4ff-afdffadb2472",
    "propertyId": "d5e1af33-cfbd-4e89-97cd-591c96eb2a60",
    "startDate": "2023-12-14",
    "endDate": "2023-12-16"
}
```

4- __Delete a block in Property__

In order to delete a block from a property we only validate that block and property exists

#### Request
```
curl --location --request DELETE 'localhost:8080/property/a257d3cf-7051-46f0-a66a-4d82ce7098f7/block/c514e0c5-6676-4e33-a8d2-c92007d67812' \
--header 'Content-Type: application/json' \
--data '{
    "startDate":"2023-12-14",
    "endDate":"2023-12-16"
}''
```
## Book Api Documentation

1- __Create a book__

In order to create a book we should provide all needed information  a block from a property we only validate that block and property exists

#### Request
```
curl --location 'localhost:8080/book' \
--header 'Content-Type: application/json' \
--data-raw '{
    "propertyId":"660200b7-7ada-494f-909f-45ac7671ff71",
    "startDate":"2023-12-23",
    "endDate":"2023-12-24",
    "guest":{
        "firstName": "Carlos",
        "lastName":"Zerga",
        "email":"zerga.morales.carlos@gmail.com",
        "phoneNumber": "993762804",
        "comment":"Lenguaje Español"
    }
}'
```
#### Response
```
{
    "id": "9b248190-f032-4780-b4e6-c3c13df7a232",
    "propertyId": "660200b7-7ada-494f-909f-45ac7671ff71",
    "status": "PENDING",
    "startDate": "2023-12-23",
    "endDate": "2023-12-24",
    "guest": {
        "firstName": "Carlos",
        "lastName": "Zerga",
        "email": "zerga.morales.carlos@gmail.com",
        "phoneNumber": "993762804",
        "comment": "Lenguaje Español"
    }
}
```


2.- __Update book dates__

In order to update a book dates we validate dates on previously booked and also blocked

#### Request
```
curl --location --request PUT 'localhost:8080/book/36d589d6-199d-4ce1-8f24-37b70ec64354/dates' \
--header 'Content-Type: application/json' \
--data '{
    "startDate":"2023-12-20",
    "endDate":"2023-12-22"
}'
```
#### Response
```
{
    "id": "36d589d6-199d-4ce1-8f24-37b70ec64354",
    "propertyId": "4b2ee1e8-b324-408d-88f2-8fd8400d60ac",
    "startDate": "2023-12-20",
    "endDate": "2023-12-22",
    "guest": {
        "firstName": "Carlos",
        "lastName": "Zerga",
        "email": "zerga.morales.carlos@gmail.com",
        "phoneNumber": "993762804",
        "comment": "Lenguaje Español"
    }
}
```

3- __Update book guest info__

In order to update a book guest info we only validate if entity exists

#### Request
```
curl --location --request PUT 'localhost:8080/book/36d589d6-199d-4ce1-8f24-37b70ec64354/guest' \
--header 'Content-Type: application/json' \
--data-raw '{
    "firstName": "Juanito",
    "lastName": "Zerga",
    "email": "zerga.morales.carlos@gmail.com",
    "phoneNumber": "993762804",
    "comment": "Lenguaje Español"
}'
```
#### Response
```
{
    "id": "36d589d6-199d-4ce1-8f24-37b70ec64354",
    "propertyId": "4b2ee1e8-b324-408d-88f2-8fd8400d60ac",
    "startDate": "2023-12-20",
    "endDate": "2023-12-22",
    "guest": {
        "firstName": "Carlos",
        "lastName": "Zerga",
        "email": "zerga.morales.carlos@gmail.com",
        "phoneNumber": "993762804",
        "comment": "Lenguaje Español"
    }
}
```

4.- __Get book__

In order to get a book

#### Request
```
curl --location 'localhost:8080/book/829d450d-fd58-4139-bf6e-b861b09ff472'
```
#### Response
```
{
    "id": "829d450d-fd58-4139-bf6e-b861b09ff472",
    "propertyId": "660200b7-7ada-494f-909f-45ac7671ff71",
    "status": "CANCELLED",
    "startDate": "2023-12-23",
    "endDate": "2023-12-24",
    "guest": {
        "firstName": "Carlos",
        "lastName": "Zerga",
        "email": "zerga.morales.carlos@gmail.com",
        "phoneNumber": "993762804",
        "comment": "Lenguaje Español"
    }
}
```

5.- __Delete book__

In order to delete a book

#### Request
```
curl --location --request DELETE 'localhost:8080/book/9b248190-f032-4780-b4e6-c3c13df7a232'
```


4.- __Cancel book__

In order to cancel a book only on PENDING stage

#### Request
```
curl --location --request POST 'localhost:8080/book/829d450d-fd58-4139-bf6e-b861b09ff472/cancel'
```
#### Response
```
{
    "id": "829d450d-fd58-4139-bf6e-b861b09ff472",
    "propertyId": "660200b7-7ada-494f-909f-45ac7671ff71",
    "status": "CANCELLED",
    "startDate": "2023-12-23",
    "endDate": "2023-12-24",
    "guest": {
        "firstName": "Carlos",
        "lastName": "Zerga",
        "email": "zerga.morales.carlos@gmail.com",
        "phoneNumber": "993762804",
        "comment": "Lenguaje Español"
    }
}
```


4- __Revive a cancelled book__

In order to revive a book only on CANCELLED stage and if dates are available

#### Request
```
curl --location --request POST 'localhost:8080/book/829d450d-fd58-4139-bf6e-b861b09ff472/revive'
```
#### Response
```
{
    "id": "829d450d-fd58-4139-bf6e-b861b09ff472",
    "propertyId": "660200b7-7ada-494f-909f-45ac7671ff71",
    "status": "CANCELLED",
    "startDate": "2023-12-23",
    "endDate": "2023-12-24",
    "guest": {
        "firstName": "Carlos",
        "lastName": "Zerga",
        "email": "zerga.morales.carlos@gmail.com",
        "phoneNumber": "993762804",
        "comment": "Lenguaje Español"
    }
}
```
